﻿using System;
using System.IO;

namespace SyscallParser
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length <= 0)
            {
                Console.WriteLine("USAGE: {0} [64|32]", AppDomain.CurrentDomain.FriendlyName);
                return;
            }

            bool x64 = args[0].Contains("64");

            //
            // Create the parser instance
            //
            SyscallParser parser = new SyscallParser(x64);
            parser.DownloadTable();

            //
            // Now generate the C++ file with defines and entries as structures
            //
            using (StreamWriter file = new StreamWriter("syscall_table.h"))
                parser.DumpTable(file);

            Console.WriteLine("Done");
            Console.ReadLine();
        }
    }
}