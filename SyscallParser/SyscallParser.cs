﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SyscallParser
{
    class SyscallParser
    {
        private class SyscallEntry
        {
            public String Name;
            public ushort[] Indices;
        }

        private String m_DownloadURL;
        private String m_Type;
        private List<SyscallEntry> m_SyscallEntries;

        public SyscallParser(bool X64)
        {
            m_SyscallEntries = new List<SyscallEntry>();

            if (X64)
            {
                m_DownloadURL = "http://j00ru.vexillium.org/ntapi_64/";
                m_Type = "64";
            }
            else
            {
                m_DownloadURL = "http://j00ru.vexillium.org/ntapi/";
                m_Type = "32";
            }
        }

        public void DownloadTable()
        {
            WebClient client = new WebClient();

            //
            // Set the user agent if needed
            //
            client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");

            using (Stream stream = client.OpenRead(m_DownloadURL))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    //
                    // HTTP response data
                    //
                    String data = reader.ReadToEnd();

                    //
                    // Each entry begins with a <tr>, so this acts as the end
                    // delimiter. The starting delimiter will be a <td> with a
                    // class containing "sym_name#"
                    //
                    // <td class="sym_name1">ArbPreprocessEntry</td>
                    // <td class="sym_name2">KeRestoreFloatingPointState</td>

                    //
                    // Find the first entry and use the substring
                    //
                    int startIndex = data.IndexOf("<td class=\"sym_name1\">");
                    data = data.Substring(startIndex);

                    //
                    // Split each newline into an array
                    //
                    String[] lines = data.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

                    for (int i = 0; i < lines.Length; i++)
                    {
                        //
                        // Limit the minimum string length (skip <tr>)
                        //
                        if (lines[i].Length <= 20)
                            continue;

                        //
                        // If the line contains 'td class' there will
                        // be a syscall name, so parse it.
                        //
                        if (lines[i].Contains("<td class="))
                            m_SyscallEntries.Add(ParseSyscallEntry(lines, ref i));
                    }
                }
            }
        }

        public void DumpTable(StreamWriter File)
        {
            File.WriteLine("#pragma once");
            File.WriteLine();

            //
            // Write the #defines
            //
            for (int i = 0; i < m_SyscallEntries.Count; i++)
                File.WriteLine("#define ID_{0} {1}", m_SyscallEntries[i].Name, i);

            File.WriteLine();

            //
            // Write the table itself
            //
            File.WriteLine("unsigned short SyscallLookupTable{0}[{1}][{2}] =", m_Type, m_SyscallEntries.Count, 16);
            File.WriteLine("{");

            foreach (var entry in m_SyscallEntries)
            {
                File.Write("\t{ ");

                foreach (var index in entry.Indices)
                    File.Write("0x{0:X4}, ", index);

                File.WriteLine("},");
            }

            File.WriteLine("};");
        }

        private static SyscallEntry ParseSyscallEntry(String[] Lines, ref int Index)
        {
            SyscallEntry entry = new SyscallEntry();

            // The first line contains the name, while each line afterwards
            // contains "text=VALUE" for the system call index.
            entry.Name = ParseSyscallName(Lines[Index++]);
            entry.Indices = new ushort[16];

            //
            // <td id="0_xx" class="syscallid2" text="&nbsp;&nbsp;&nbsp;" name="os_0"></td>
            // <td id="0_xx" class="syscallid2" text="&nbsp;VALUE&nbsp;" name="os_0"></td>
            //
            for (int i = 0; i < entry.Indices.Length; i++)
                entry.Indices[i] = ParseSyscallIndex(Lines[Index + i]);

            Index += entry.Indices.Length;
            return entry;
        }

        private static String ParseSyscallName(String Input)
        {
            int startIndex = Input.IndexOf("<td class=\"sym_") + 22;
            int endIndex = Input.IndexOf("</td>");

            return Input.Substring(startIndex, endIndex - startIndex);
        }

        private static ushort ParseSyscallIndex(String Input)
        {
            int startIndex = Input.IndexOf("\"&nbsp;") + 1;
            int endIndex = Input.IndexOf("&nbsp;\"");

            //
            // Strip "&nbsp;" from the line
            //
            String value = Input.Substring(startIndex, endIndex - startIndex).Replace("&nbsp;", "");

            //
            // A length of 0 indicates the index did not exist
            //
            if (value.Length <= 0)
                return 0xFFFF;

            //
            // Remove "0x" prefix
            //
            value = value.Replace("0x", "");

            //
            // Try to parse the hex value
            //
            return ushort.Parse(value, System.Globalization.NumberStyles.HexNumber);
        }
    }
}